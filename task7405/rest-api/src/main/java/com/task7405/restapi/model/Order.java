package com.task7405.restapi.model;

import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

@Entity
@Table(name = "orders")
public class Order {
    @Id
    @Column(name = "order_number")
    private int orderNumber;

    @Temporal(TemporalType.TIMESTAMP)

    @Column(name = "order_date", nullable = true)
    private Date orderDate;
    @Column(name = "require_date", nullable = true)
    private Date requireDate;
    @Column(name = "shipper_date", nullable = true)
    private Date shipperDate;

    @Column(name = "status")
    private String status;
    @Column(name = "comment")
    private String comment;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "customer_number", nullable = false, referencedColumnName = "customer_number")
    private Customer customer;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "orders")
    private Set<Orderdetail> orderDetail;

}
