package com.task7405.restapi.model;

import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

@Entity
@Table(name = "product")
public class Product {
    @Id
    @Column(name = "product_code")
    private String productCode;
    @Column(name = "product_name")
    private String productName;

    @Column(name = "product_scale")
    private String productScale;
    @Column(name = "product_vendor")
    private String productVendor;
    @Column(name = "product_description")
    private String productDescription;
    @Column(name = "quantity_in_stock")
    private int quantityInStock;
    @Column(name = "buy_price")
    private float buyPrice;
    @Column(name = "MSRP")
    private float msrp;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "product")
    private Set<Orderdetail> orderDetail;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "product_line", nullable = false, referencedColumnName = "product_line")
    private ProductLine productLine;

}
